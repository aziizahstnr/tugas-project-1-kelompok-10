<?php
session_start();

// Data kursus disimpan dalam array (contoh)
$courses = array(
    array("Course Title 1", "Description 1", "Material 1", "Assessment 1"),
    array("Course Title 2", "Description 2", "Material 2", "Assessment 2"),
    array("Course Title 3", "Description 3", "Material 3", "Assessment 3"),
    // Tambahkan data kursus lainnya jika diperlukan
);

// Check if user is not logged in, redirect to login page
if (!isset($_SESSION['user'])){
  header('Location: login.html');
  exit();
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EduTechHub - Home</title>
    <link rel="stylesheet" href="style.css">
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        th {
            background-color: #f2f2f2;
        }
        /* Style untuk tautan pada baris tabel */
        tr:hover {
            background-color: #f5f5f5;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <header>
        <nav class="navbar">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="courses.php">Courses</a></li>
                <li><a href="Logout.php">Logout</a></li>
            </ul>
        </nav>
    </header>
    <main>
        <section class="course-list">
            <h2>Available Courses</h2>
            <!-- Tabel untuk menampilkan daftar kursus -->
            <table>
                <thead>
                    <tr>
                        <th>Course Title</th>
                        <th>Description</th>
                        <th>Material</th>
                        <th>Assessment</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // Loop untuk menampilkan data kursus dalam tabel
                    foreach ($courses as $course) {
                        // Mendapatkan judul kursus untuk digunakan dalam tautan
                        $courseTitle = $course[0];
                        // Menambahkan tautan pada setiap baris tabel yang mengarah ke halaman detail kursus
                        echo "<tr onclick=\"window.location='detail_course1.php?title=$courseTitle';\">";
                        foreach ($course as $detail) {
                            echo "<td>$detail</td>";
                        }
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
        </section>
    </main>
    <footer>
        <p>&copy; 2024 EduTechHub. All rights reserved.</p>
    </footer>
</body>
</html>
